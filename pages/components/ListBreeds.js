import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

class ListBreeds extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const breedList = Object.keys(this.props.breeds || {});
    return (
      <ScrollView>
        <View>
          <View style={styles.titleHeader}>
            <Text style={styles.titleLabel}>List of Breeds</Text>
          </View>
          {breedList.map((breed, key) => {
            return (
              <View>
                <TouchableOpacity style={styles.item} onPress={() => { this.props.pressBreed(breed); }}>
                  <Text style={styles.label}>{breed}</Text>
                </TouchableOpacity>
              </View>
            );
          })
        }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    color: '#fff',
    fontSize: 20,
  },
  item: {
    height: 45,
  },
  titleHeader: {
    marginBottom: 20,
    backgroundColor: 'rgba(92, 99,216, 1)',
    paddingHorizontal: 40,
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
  },
  titleLabel: {
    color: '#fff',
    fontSize: 20,
  },
});

const mapStateToProps = (state) => {
  return {
    dog_breed: state.breed,
    dog_url: state.url,
    breeds: state.breeds,
  };
};

export default connect(mapStateToProps)(ListBreeds);
