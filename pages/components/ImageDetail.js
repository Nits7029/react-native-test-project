import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

class ImageDetail extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <View>
        <View> <Image
          style={styles.dogImage}
          source={this.props.dog_url && { uri: this.props.dog_url }}
        />
          <Text style={styles.label}>{'Look at the dog!'}</Text>
          <TouchableOpacity style={styles.button} onPress={() => { this.props.pressBack(); }}>
            <Text style={styles.label}>BACK</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dogImage: {
    width: 200,
    height: 200,
    marginBottom: 50,
  },
  label: {
    color: '#fff',
    fontSize: 20,
  },
  button: {
    backgroundColor: 'rgba(92, 99,216, 1)',
    width: 200,
    height: 45,
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
});

const mapStateToProps = (state) => {
  return {
    dog_breed: state.breed,
    dog_url: state.url,
    breeds: state.breeds,
  };
};

export default connect(mapStateToProps)(ImageDetail);
