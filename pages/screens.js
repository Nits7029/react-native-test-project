import { Navigation } from 'react-native-navigation';
import Main from "./Main";

export function register_screens(store, Provider) {

  Navigation.registerComponent(
    'inkind.test.Main',
    () => Main,
    store,
    Provider
  );

};
