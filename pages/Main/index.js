import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { get_random_dog, get_all_breeds } from '../../actions/dog';
import ImageDetail from '../components/ImageDetail';
import ListBreeds from '../components/ListBreeds';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listScreenDisplay: true,
      imageScreenDisplay: false,
    };
    this._onPressBreed = this._onPressBreed.bind(this);
    this._onBackPress = this._onBackPress.bind(this);
  }
  componentWillMount(props) {
    this.props.dispatch(get_all_breeds());
  }
  _onPressBreed(breed) {
    this.setState({
      listScreenDisplay: false,
      imageScreenDisplay: true,
    });
    this.props.dispatch(get_random_dog(breed));
  }
  _onBackPress() {
    this.setState({
      listScreenDisplay: true,
      imageScreenDisplay: false,
    });
  }
  render() {
    return (
      <View style={styles.dogWrapper}>
        {this.state.listScreenDisplay && <ListBreeds pressBreed={this._onPressBreed} />}
        {this.state.imageScreenDisplay && <ImageDetail pressBack={this._onBackPress} />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dogWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#2d324c',
    paddingTop: 100,
  },
});

const mapStateToProps = (state) => {
  return {
    dog_breed: state.breed,
    dog_url: state.url,
    breeds: state.breeds,
  };
};

export default connect(mapStateToProps)(Main);
