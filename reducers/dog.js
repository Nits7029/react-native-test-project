import { GET_RANDOM_DOG,GET_ALL_BREEDS } from "../actions/types";

const defaultState = {
  breed: null,
  url: null,
  breeds:null
};

export default function reducer(state = defaultState, action) {

  switch (action.type) {
    case GET_RANDOM_DOG: {
      return {
        ...state,
        breed: "random",
        url: action.url
      };
    }
    case GET_ALL_BREEDS: {
      return {
        ...state,
        breeds: action.breeds,
      };
    }
    default:
      return state;
  };
};
