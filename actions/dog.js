import { GET_RANDOM_DOG,GET_ALL_BREEDS } from "./types";

export const saveRandomDog = (url) => ({
  type: GET_RANDOM_DOG,
  url
});

export const get_random_dog = (breed) => {
  return (dispatch, getState) => {
    dispatch(saveRandomDog('https://image.ibb.co/e47pS8/favicon_194x194.png'))
    fetch("https://dog.ceo/api/breed/"+breed+"/images/random", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveRandomDog(responseJSON.message));
        })
    })
  };
};

export const saveAllBreeds = (breeds) => ({
  type: GET_ALL_BREEDS,
  breeds
});

export const get_all_breeds = () => {
  return (dispatch, getState) => {
    fetch("https://dog.ceo/api/breeds/list/all", {
      method: "GET"
    }).then(response => {
      response.json()
        .then(responseJSON => {
          dispatch(saveAllBreeds(responseJSON.message));
        })
    })
  };
};
